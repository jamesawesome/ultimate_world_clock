import time
import math

from adafruit_bitmap_font import bitmap_font

colors = {
    'blue': 0x001BFF,
    'yellow': 0xFFFF00,
    'green': 0x00FF1B,
    'red': 0xFF0000,
    'purple': 0xFF00FF,
}

ROW_1_Y = 6
ROW_2_Y = 13
ROW_3_Y = 20
ROW_4_Y = 27

SMALL_FONT = bitmap_font.load_font('/fonts/4x6.bdf')

def find_center(canvas_width, text_width):
    return int((canvas_width / 2) - math.floor(text_width / 2))

def sync_time(network):
    retries = 0
    while retries <= 5:
        try:
            network.get_local_time()
            return time.monotonic()
        except Exception as err:
            print("Error occured while syncing time, retrying! -", err)
            retries += 1

    raise Exception('Failed to sync time!')
