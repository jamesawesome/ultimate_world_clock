#!/usr/bin/env bash

export OPERATION=$1
case $OPERATION in
    sync)
        rsync -avz --exclude '.*' --exclude 'lib' --exclude 'boot_out.txt' /Volumes/CIRCUITPY/ ./
    ;;
    deploy)
        rsync -avz --exclude '.*' --exclude 'lib' --exclude 'build.sh' --exclude '*.md' ./ /Volumes/CIRCUITPY/
    ;;
    *)
        echo "Invalid operation ${OPERATION}"
        exit 1
    ;;
esac
