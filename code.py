import time
import board
import busio
import displayio

from adafruit_matrixportal.network import Network
from adafruit_matrixportal.matrix import Matrix
import adafruit_lis3dh

from widgets import DateWidget, LedClock, WeatherWidget
from utils import (
    colors,
    ROW_1_Y,
    ROW_2_Y,
    ROW_3_Y,
    ROW_4_Y,
    SMALL_FONT,
    sync_time,
)

try:
    from secrets import secrets
except ImportError:
    print('WiFi secrets are kept in secrets.py, please add them there!')
    raise

BITPLANES = 6      # Ideally 6, but can set lower if RAM is tight

# ONE-TIME INITIALIZATION --------------------------------------------------

MATRIX = Matrix(bit_depth=BITPLANES)
DISPLAY = MATRIX.display

NETWORK = Network(status_neopixel=board.NEOPIXEL, debug=False)

ACCEL = adafruit_lis3dh.LIS3DH_I2C(busio.I2C(board.SCL, board.SDA),
                                   address=0x19)
_ = ACCEL.acceleration # Dummy reading to blow out any startup residue

time.sleep(0.1)

LAST_TIME_SYNC = sync_time(NETWORK)

############################################################################


GMT_CLOCK = LedClock(SMALL_FONT, colors['blue'], 0, 'UTC', y=ROW_1_Y, x=32)
EST_CLOCK = LedClock(SMALL_FONT, colors['yellow'], -5, 'EST', y=ROW_2_Y, x=32)
CST_CLOCK = LedClock(SMALL_FONT, colors['green'], -6, 'CST', y=ROW_3_Y, x=32)
PST_CLOCK = LedClock(SMALL_FONT, colors['red'], -8, 'PST', y=ROW_4_Y, x=32)

all_clocks = [GMT_CLOCK, EST_CLOCK, CST_CLOCK, PST_CLOCK]

date = DateWidget(NETWORK, location='America/New_York')
weather = WeatherWidget(SMALL_FONT, colors['purple'], colors['blue'], '40.0608634', '-75.0961545')

GROUP = displayio.Group()
weather.add_to_group(GROUP)
date.add_to_group(GROUP)

for clock in all_clocks:
    clock.add_to_group(GROUP)


###
# Main
###

DISPLAY.show(GROUP)

while True:
    now = time.localtime()
    for clock in all_clocks:
        clock.update(now)

    # Update Date
    if time.localtime().tm_min == 0:
        date.update()

    # Incase we missed an update at the top of the hour, update.
    if time.monotonic() > date.last_update_time + 3600:
        date.update()

    # Update Weather every half hour
    if time.monotonic() > weather.last_update_time + 1800:
        weather.update()

    # Sync Time
    if time.monotonic() > LAST_TIME_SYNC + 7200:
        try:
            LAST_TIME_SYNC = sync_time(NETWORK)
        except Exception as e:
            # Let this retry next loop
            pass

    time.sleep(1)
