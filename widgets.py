import gc
import time

import adafruit_display_text.label
import adafruit_requests as requests

from utils import (
    colors,
    ROW_1_Y,
    ROW_2_Y,
    ROW_3_Y,
    ROW_4_Y,
    SMALL_FONT,
    find_center,
)

from secrets import secrets


def make_celsius(fahrenheit):
    return str(int(round((int(fahrenheit) - 32) * 5.0/9.0)))


class LedClock():

    def __init__(self, font, color, offset, tz_name, y=0, x=32, dst=False, dst_name=None, twelve_hour=False):
        self.offset = offset
        self.tz_name = tz_name
        self.dst = dst
        self.dst_name = dst_name
        self.twelve_hour = twelve_hour
        self.label = adafruit_display_text.label.Label(
            font, color=color, y=y, x=x
        )
        self.tz_label = adafruit_display_text.label.Label(
            font, color=color, y=y, x=(x + 21)
        )

        self.update(time.localtime())

    @property
    def labels(self):
        for label in [self.label, self.tz_label]:
            yield label

    def add_to_group(self, group):
        for label in self.labels:
            group.append(label)

    def _format_time(self, time_struct):
        """ Given a time.struct_time, return a string as H:MM or HH:MM, either
            12- or 24-hour style depending on global TWELVE_HOUR setting.
            This is ONLY for 'clock time,' NOT for countdown time, which is
            handled separately in the one spot where it's needed.
        """
        offset_tm_hour = (time_struct.tm_hour + self.offset) % 24
        if self.twelve_hour:
            if offset_tm_hour > 12:
                hour_string = str(offset_tm_hour - 12) # 13-23 -> 1-11 (pm)
            elif offset_tm_hour > 0:
                hour_string = str(offset_tm_hour) # 1-12
            else:
                hour_string = '12' # 0 -> 12 (am)
        else:
            hour_string = '{0:0>2}'.format(offset_tm_hour)
        return hour_string + ':' + '{0:0>2}'.format(time_struct.tm_min)


    def update(self, now):
        tz_name = self.tz_name
        if self.dst and self.dst_name:
            tz_name = self.dst_name

        self.label.text = self._format_time(now)
        self.tz_label.text = tz_name


class DateWidget():

    def __init__(self, network, location='America/New_York'):
        self.network = network
        self.location = location
        self.year = adafruit_display_text.label.Label(SMALL_FONT, color=colors['red'], y=ROW_1_Y, x=2)
        self.month = adafruit_display_text.label.Label(SMALL_FONT, color=colors['purple'], y=ROW_1_Y, x=19)
        self.day_of_week = adafruit_display_text.label.Label(SMALL_FONT, color=colors['blue'], y=ROW_2_Y, x=6)
        self.day_of_month = adafruit_display_text.label.Label(SMALL_FONT, color=colors['green'], y=ROW_2_Y, x=19)
        self.update()

    def update(self):
        retries = 0
        while retries <= 5:
            try:
                date = self.network.get_strftime('%Y %b %a %d', location=self.location).split()

                self.year.text = date[0]
                self.month.text = date[1]
                self.day_of_week.text = date[2]
                self.day_of_month.text = date[3]

                self.last_update_time = time.monotonic()

                # Exit
                gc.collect()
                return

            except Exception as err:
                print('Error while updating date "' + str(err) + '"')

                # Backoff...
                retries += 1
                time.sleep(2 * retries)

                # Retries can make a lot of garbage
                gc.collect()

    @property
    def labels(self):
        for label in [self.year, self.month, self.day_of_week, self.day_of_month]:
            yield label

    def add_to_group(self, group):
        for label in self.labels:
            group.append(label)



class WeatherWidget():

    def __init__(self, font, color, weather_color, lat, lon):
        self.weather_params = {
            "units": "imperial",
            "exclude": [
                'minutely',
                'hourly',
                'daily',
                'alerts'
            ],
            'appid': secrets['openweathermap_key'],
        }
        self.weather_params['lat'] = lat
        self.weather_params['lon'] = lon

        self.temp_label_f = adafruit_display_text.label.Label(
            font, color=colors['yellow'], y=ROW_3_Y, x=0 # 12 is width of text
        )

        self.temp_label_c = adafruit_display_text.label.Label(
            font, color=color, y=ROW_3_Y, x=0 # 12 is width of text
        )

        self.weather_label = adafruit_display_text.label.Label(
            font, color=weather_color, y=ROW_4_Y, x=0 # 12 is width of text
        )

        self.update()


    @property
    def labels(self):
        for label in [self.weather_label, self.temp_label_f, self.temp_label_c]:
            yield label


    def update(self):
        print('Updating Weather...')
        res = None
        try:
            res = requests.get(
                '{}?appid={}&units={}&lat={}&lon={}&exclude={}'.format(
                    'https://api.openweathermap.org/data/2.5/onecall',
                    self.weather_params['appid'],
                    self.weather_params['units'],
                    self.weather_params['lat'],
                    self.weather_params['lon'],
                    ','.join(self.weather_params['exclude']),
                )
            )

            if res.status_code != 200:
                print('Non-2xx Response: ' + str(res.json()))
                # TODO: Display error or something here (NaN?)
                #
                # Prevent rapid fire updates by setting the updated time as now
                self.last_update_time = time.monotonic()
                return

            current_temp = str(res.json()['current']['temp']).split('.')[0]
            weather = res.json()['current']['weather'][0]['main']

        except Exception as err:
            print('Error while updating weather "' + str(err) + '"')
            self.last_update_time = time.monotonic()
            return

        finally:
            del res
            gc.collect()

        # "ThunderStorm" is too long for the screen
        if weather == 'Thunderstorm':
            weather == 'Thunder'

        current_temp_f = current_temp + 'F'
        current_temp_c = make_celsius(current_temp) + 'C'

        start_x = find_center(32, (len(current_temp_f + current_temp_c) * 4))

        self.temp_label_f.text = current_temp_f
        self.temp_label_f.x = start_x

        self.temp_label_c.text = current_temp_c
        self.temp_label_c.x = (start_x + (len(current_temp_f) * 4))

        self.weather_label.text = weather
        self.weather_label.x = find_center(32, (len(weather) * 4))

        self.last_update_time = time.monotonic()
        print('Successfully updated weather!')

    def add_to_group(self, group):
        for label in self.labels:
            group.append(label)
